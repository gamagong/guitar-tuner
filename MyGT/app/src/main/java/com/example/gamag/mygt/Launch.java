package com.example.gamag.mygt;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.HorizontalScrollView;
import android.widget.TextView;

import com.example.gamag.mygt.Math.PitchDetectionResult;
import com.example.gamag.mygt.Utils.Note;
import com.github.shchurov.horizontalwheelview.HorizontalWheelView;

import java.util.Locale;



public class Launch extends AppCompatActivity {
    //колесо вертит ноты
    //обновление стрелки от частоты
    //лок и анлок ноты, чекбокс какой-нибудь

    private HorizontalWheelView wheel; //модель колеса
    private int displayWidth;//ширина дисплея
    private Button[] notes; //модель нот
    private int[] id = {0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330};//массив адишников нот
    private String[] notesSign = {"A", "A♯", "B", "C", "C♯", "D", "D♯", "E", "F", "F♯", "G", "G♯"};//массив символов нот
    private HorizontalScrollView sw;//модель скролла

    private int PERMISSION_REQUEST_CODE = 1;
    private Tuner tuner;
    private ImageView arrow;

    private int prevID;
    private TextView textView;

    private boolean manual = false;
    private CheckBox cb_manual;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        initViews();
        setupListeners();
        requestPermissions();
        arrow = (ImageView) findViewById(R.id.arrow);
        arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateArrow();
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
    }

    private void initViews() {
        wheel = (HorizontalWheelView) findViewById(R.id.horizontalWheelView);
        wheel.setMarksCount(12);
        wheel.setOnlyPositiveValues(true);
        wheel.setSnapToMarks(true);
        prevID = 0;

        cb_manual = (CheckBox) findViewById(R.id.cb_manual);
        cb_manual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (manual) {
                    manual = false;
                } else {
                    manual = true;
                }
            }
        });

        sw = (HorizontalScrollView) findViewById(R.id.notes_scrl);
        sw.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        textView = (TextView) findViewById(R.id.textView);
        notes = new Button[12];
        String buttonID;
        int resID;
        Display display = getWindowManager().getDefaultDisplay();
        displayWidth = display.getWidth();
        for (int i = 0; i < notes.length; i++) {
            buttonID = "note_" + i;
            resID = getResources().getIdentifier(buttonID, "id", getPackageName());
            notes[i] = (Button) findViewById(resID);
        }
    }

    private String getNotebyDegree(double degrees) {
        int intDegrees = (int) degrees;
        if (intDegrees % 10 != 0) {
            intDegrees++;
        }
        Log.d("Notes", "ID " + String.valueOf(intDegrees));
        for (int i = 0; i < id.length; i++) {
            if (intDegrees == id[i]) {
                return notesSign[i];
            }
        }
        return "";
    }

    private void updateText() {
        String text = String.format(Locale.US, "%.0f°", wheel.getDegreesAngle());
        textView.setText(text);
    }

    private void setupListeners() {
        wheel.setListener(new HorizontalWheelView.Listener() {
            @Override
            public void onRotationChanged(double radians) {
                updateText();
                updateNotes(getNotebyDegree(wheel.getDegreesAngle()));
            }
        });
    }

    public void updateLaunch(float Pitch, String note) {
        if (!manual) {
            updateNotes(note);
        }

    }

    private int getNoteID(String note) {
        for (int i = 0; i < notesSign.length; i++) {
            if (notesSign[i].equals(note)) {
                return i;
            }
        }
        return -1;
    }

    private void updateNotes(String note) {
        int id = getNoteID(note);
        Log.d("Notes", "ID " + String.valueOf(id));
        if (id != -1 && prevID != id) {
            int scrollX = (notes[id].getLeft() - (displayWidth / 2)) + (notes[id].getWidth() / 2);
            sw.smoothScrollTo(scrollX, 0);
            notes[prevID].setTextColor(Color.BLACK);
            notes[id].setTextColor(Color.YELLOW);
            Log.d("Coords", String.valueOf(notes[prevID].getLeft()) + " " + String.valueOf(notes[id].getLeft()));
            prevID = id;
        }
    }

    public void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{
                        Manifest.permission.RECORD_AUDIO
                },
                PERMISSION_REQUEST_CODE);
    }

    //Запрос пермишена
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE && grantResults.length == 1) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                finish();
                startActivity(new Intent(this, PermissonActivity.class));
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    //После возвращения к тюнеру
    @Override
    protected void onResume() {
        if (tuner != null && tuner.isInitialized()) {
            tuner.start();
        }
        super.onResume();
    }

    //Переключение с тюнера
    @Override
    protected void onPause() {
        if (tuner != null && tuner.isInitialized()) {
            tuner.stop();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (tuner != null && tuner.isInitialized()) {
            tuner.stop();
            tuner.release();
        }
        super.onDestroy();
    }
    @Override
    protected void onStart(){
        super.onStart();
        tuner = new Tuner(new TunerUpdate() {
            @Override
            public void updateNote(Note newNote, PitchDetectionResult result) {
                updateLaunch(result.getPitch(),newNote.getNote());
            }
        });
    }
}
